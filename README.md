**Analisador Léxico - Calculadora**

**Dupla:** Darlan Nakamura e Rafael Bezerra

**Questões a considerar:**

**1. Que símbolo usar como separador de casa decimais?**
R: Estamos utilizando o (ponto).

**2. A calculadora usa representação monetária?**
R: Não.

**3. A calculadora aceita espaços entre os operandos e operadores?**
R: Sim.
